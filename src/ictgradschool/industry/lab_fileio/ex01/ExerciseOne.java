package ictgradschool.industry.lab_fileio.ex01;

import java.io.*;

public class ExerciseOne {

    public void start() {

        printNumEsWithFileReader();

        printNumEsWithBufferedReader();

    }

    private void printNumEsWithFileReader() {

        int numE = 0;
        int total = 0;

        // TODO Read input2.txt and print the total number of characters, and the number of e and E characters.
        // TODO Use a FileReader.
        File myFile = new File("input2.txt");
        try( FileReader itsMine = new FileReader( myFile)){

           int character;
           while((character=itsMine.read())!=-1){
               System.out.println((char) character);
               total++;
               if((char) character=='E'||character==(int) 'e'){
                   numE++;
               }
           }



        }catch(IOException e){
            System.out.println("Error: " + e.getMessage());
        }
        System.out.println("Number of e/E's: " + numE + " out of " + total);
    }

    private void printNumEsWithBufferedReader() {

        int numE = 0;
        int total = 0;

        // TODO Read input2.txt and print the total number of characters, and the number of e and E characters.
        // TODO Use a BufferedReader.
        File myFile = new File("input2.txt");
        try(BufferedReader itsMySecondOne= new BufferedReader(new FileReader(myFile))){
            String myLine = null;
            while((myLine = itsMySecondOne.readLine()) != null){
                System.out.println(myLine.length());
                for(int i=0;i<myLine.length();i++){
                    total++;
                    if(myLine.charAt(i)=='e'||myLine.charAt(i)=='E'){
                        numE++;
                    }
                }
            }
        }
        catch(IOException e){}

        System.out.println("Number of e/E's: " + numE + " out of " + total);
    }

    public static void main(String[] args) {
        new ExerciseOne().start();
    }

}
