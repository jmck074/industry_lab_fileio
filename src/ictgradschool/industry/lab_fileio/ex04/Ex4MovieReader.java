package ictgradschool.industry.lab_fileio.ex04;

import ictgradschool.industry.lab_fileio.ex03.Movie;
import ictgradschool.industry.lab_fileio.ex03.MovieReader;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Scanner;

/**
 * Created by anhyd on 20/03/2017.
 */
public class Ex4MovieReader extends MovieReader {

    @Override
    protected Movie[] loadMovies(String fileName) {
        Movie [] movieArray;

      // TODO Implement this with a Scanner

        System.out.println("Step One");
        try (Scanner myScanner = new Scanner(new File(fileName))){

        myScanner.useDelimiter(",|\\r\\n");

            //int arraylength = myScanner.nextInt();
            //System.out.println("Array length is: " + arraylength);
            movieArray=new Movie[19];
            int i =0;

            while (myScanner.hasNext()){
                String Name = myScanner.next();
                int Year = Integer.parseInt(myScanner.next());
                int Length = myScanner.nextInt();
                String Director = myScanner.next();


                Movie currentMovie = new Movie(Name, Year, Length, Director );
                movieArray[i]=currentMovie;
                i++;
                System.out.println(i);

            }
        }catch (FileNotFoundException e) {
            e.printStackTrace();
            movieArray=null;
        }


        return movieArray;
    }

    public static void main(String[] args) {
        new Ex4MovieReader().start();
    }
}
