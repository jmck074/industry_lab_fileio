package ictgradschool.industry.lab_fileio.ex04;

import ictgradschool.industry.lab_fileio.ex03.Movie;
import ictgradschool.industry.lab_fileio.ex03.MovieWriter;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * Created by anhyd on 20/03/2017.
 */
public class Ex4MovieWriter extends MovieWriter {

    @Override
    protected void saveMovies(String fileName, Movie[] films) {

        // TODO Implement this with a PrintWriter
        try (PrintWriter myPrintWriter = new PrintWriter(new FileWriter(fileName))) {
            //myPrintWriter.println(films.length+",");
            for (Movie movie : films) {
                String oneLineAtATime = "";
                oneLineAtATime = oneLineAtATime + movie.getName() + ",";
                oneLineAtATime = oneLineAtATime + movie.getYear() + ",";
                oneLineAtATime = oneLineAtATime + movie.getLengthInMinutes() + ",";
                oneLineAtATime = oneLineAtATime + movie.getDirector();
                System.out.println(oneLineAtATime);
                myPrintWriter.println(oneLineAtATime);

            }
        }
        catch(IOException e){
            System.out.println(e.getMessage());
        }
        System.out.println("File is written. I think....");
    }

    public static void main(String[] args) {
        new Ex4MovieWriter().start();
    }

}
