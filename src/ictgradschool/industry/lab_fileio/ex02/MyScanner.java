package ictgradschool.industry.lab_fileio.ex02;

import ictgradschool.Keyboard;

import java.io.*;
import java.util.Scanner;

public class MyScanner {

    public void start() {

        // TODO Prompt the user for a file name, then read and print out all the text in that file.
        System.out.println("Enter a filename to read:");
        String ourFileName = Keyboard.readInput();

        // TODO Use a Scanner.
        try(Scanner myScannerDarkly = new Scanner(new FileReader(ourFileName))){
            while(myScannerDarkly.hasNext()){
                String aStringName = myScannerDarkly.next();
                System.out.println(aStringName);
            }
        }
        catch (IOException e){
            System.out.println("Something"+e.getMessage());
        }
        }


    public static void main(String[] args) {
        new MyScanner().start();
    }
}
