package ictgradschool.industry.lab_fileio.ex02;

import ictgradschool.Keyboard;

import java.io.*;

public class MyReader {

    public void start() {

        // TODO Prompt the user for a file name, then read and print out all the text in that file.
        System.out.println("Please enter a filename:");
        String theNameOfMyFile = Keyboard.readInput();

        // TODO Use a BufferedReader.
        try (BufferedReader theNameOfMyReader = new BufferedReader(new FileReader(theNameOfMyFile))) {
            String aLine;
            while ((aLine = theNameOfMyReader.readLine()) != null) {
                //aLine = theNameOfMyReader.readLine(); //This line is redundant or rather causes a new line to be read when the while condition has already done this so it appears to skip lines
                System.out.println(aLine);
            }
        } catch (IOException e)

        {
            System.out.println("Error:" + e.getMessage());
        }
    }

    public static void main(String[] args) {
        new MyReader().start();
    }
}
